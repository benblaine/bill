# Bill

Update: I've started testing out [line-by-line receipt capture](https://youtu.be/LQF_U-tT5z0) with the Excel app on iOS.

## About
This section is about my Friend “Bill” who reminds me to capture my receipts, and how he came to be.

**Problem**
I didn't know if I was spending too much money on cheese - and the info is lost with the receipts I wantonly discard.

**Why solve this**
Figure out if I’m “over-cheesing” by recording what I by, by capturing the items line-by-line on my receipts.

**Solution**
My Friend “Bill” who reminds me to capture my receipts. Once captured I can use them to figure out where my money is going... but that's for another day.

**Tech**
Investec Programmable Card, Telegram Bot API, Email, Wave

## How it's done

## Creating your own Telegram Bot

1. If you don't have it already, get [Telegram](https://telegram.org/) and create an account.
2. Open Telegram and search for `@botfather`
3. Type "/newbot"
4. Give your bot a name. I called mine Bill. You will also need to give it a unique username, ending with "bot".
5. Once you've found a unique name, the bot will be created and you will be shown your Bot Access Token to use with HTTP. Save your token for use later. I'll refer to it as `telegramReceiptBotToken` below.

<img src='https://gitlab.com/benblaine/bill/-/raw/master/Screenshot_2020-08-21_at_13.45.25.png'></img>

## Getting your Telegram ChatID

1. Go to your bot on Telegram and type "start" then send it any message.
2. Open your terminal and type `curl https://api.telegram.org/bot$(telegramReceiptBotToken)/getUpdates`. Take note to replace `telegramReceiptBotToken` in the URL.
3. In the response you will find your Telegram ChatID. Look for `"chat":{"id":`. Save your chat ID for later. I'll refer to it as `telegramChatID` below.

## Setting up a method of recording receipts digitally.

For now I've settled on using [Wave](https://www.waveapps.com/receipts) to store my receipts. To capture a receipt you can simply send a scan/photo to receipts@waveapps.com (once your Wave account is set up). To do this I just use the Email App on iPhone, which actually has a scanner.

TODO: I'd love to figure out how to simply reply to Bill on Telegram with a photo of my receipt!

## Set up your Telegram Bot to ping you when you use your Investec Card

1. Log into Programmable Banking on [Investec Online](https://www.investec.com/en_za.html). Go to the card/s that you want to be notitfied for, whenever they're used to make a transaction.
2. In your `main.js` file add the `telegramReceiptBot()` function from [`telegramReceiptBot.js`](https://gitlab.com/benblaine/bill/-/blob/master/telegramReceiptBot.js)
3. Don't forget to call the function in the `afterTransaction()` function in `main.js` as below.
```
const afterTransaction = async (transaction) => {
     await telegramReceiptBot(transaction);
};
```
4. Deploy to card and simulate a transaction! You should receive a message from your receipt bot.

<img src="https://gitlab.com/benblaine/bill/-/raw/master/26011322-343C-4DD4-B5D7-B744ADDA006D.jpeg" width=400></img>

## Saving your receipts with your Receipt Bot
Next time you buy something you can save receipts [as shown here](https://gitlab.com/benblaine/bill/-/blob/master/My_Movie_5.mp4).

[<img src="https://gitlab.com/benblaine/bill/-/raw/master/Screenshot_2020-08-27_at_11.23.20.png" width=400></img>](https://gitlab.com/benblaine/bill/-/blob/master/My_Movie_5.mp4)