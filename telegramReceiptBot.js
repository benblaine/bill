async function telegramReceiptBot(transaction) {
  const response = await fetch(`https://api.telegram.org/bot${process.env.telegramReceiptBotToken}/sendMessage`, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({chat_id: `${process.env.telegramChatID}`, text: `🙋 Hi - you spent ${helpers.format.decimal(transaction.centsAmount / 100, 100)} ${transaction.currencyCode} at ${transaction.merchant.name}! **Send receipt to Wave: receipts@waveapps.com.**`, parse_mode: 'Markdown' }),
  })
}